The source code for the paper:
LETRIST: Locally Encoded Transform Feature Histogram for Rotation-Invariant Texture Classification, 
by Tiecheng Song, Hongliang Li, Fanman Meng, Qingbo Wu, and Jianfei Cai, 
IEEE TCSVT, submitted, 
stc1984@126.com
version 1.0 (2016.11)

=====================================
        How to use the code
=====================================
Environment: Windows 7, Matlab R2013a 

1. Download the datasets, e.g., Outex_TC_00010, Outex_TC_00012 and CUReT.
   The Outex dataset can be downloaded from http://www.outex.oulu.fi/index.php?page=classification
   The CUReT dataset can be downloaded from http://www.robots.ox.ac.uk/~vgg/research/texclass/index.html
2. Run demo_TC10, demo_TC12t, demo_TC12h, and demo_CURET.